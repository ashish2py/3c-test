package com.example.c_test;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import fragment.*;

public class FragmentMainActivity extends SherlockFragmentActivity implements FragHeader.OnHeadlineSelectedListener{
	
	
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.frag_main_layout);

		fragmentHeaderAdd();
		fragmentListAdd();
		
	}
	
	/*
	*
	*
	*
	*	key handler needs to be imlpementd
	*	Descriable
	*
	*
	*/

    public void fragmentHeaderAdd(){
        FragmentManager fm       = getSupportFragmentManager();
        android.support.v4.app.Fragment fragment = fm.findFragmentById(R.id.fragmentListContent); 
        if (fragment == null) {
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.fragmentListContent, new FragList());	            
            ft.commit(); // Make sure you call commit or your Fragment will not be added. 
        }//end of fragment
    }    

    public void fragmentListAdd(){
        FragmentManager fm       = getSupportFragmentManager();
        android.support.v4.app.Fragment fragment = fm.findFragmentById(R.id.fragmentHeaderContent); 
        if (fragment == null) {
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.fragmentHeaderContent, new FragHeader());	            
            ft.commit(); // Make sure you call commit or your Fragment will not be added. 
        }//end of fragment
    }



	@Override
	public void onButtonClicked(int position) {
		// TODO Auto-generated method stub

		try{
			if(position == 1){
				Toast.makeText(FragmentMainActivity.this, "clicked button1", Toast.LENGTH_SHORT).show();
			}else if(position ==2){
				Toast.makeText(FragmentMainActivity.this, "clicked button2", Toast.LENGTH_SHORT).show();
			}else if(position ==3){
				Toast.makeText(FragmentMainActivity.this, "clicked button3", Toast.LENGTH_SHORT).show();				
			}else if(position ==4 ){
				Toast.makeText(FragmentMainActivity.this, "clicked button4", Toast.LENGTH_SHORT).show();								
			}
		}catch(Exception ie){
			Toast.makeText(FragmentMainActivity.this, "clicked button1" + ie, Toast.LENGTH_SHORT).show();
		}
	}



	
}
