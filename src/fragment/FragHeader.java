package fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.c_test.R;

public class FragHeader extends Fragment{

	OnHeadlineSelectedListener mCallback;

	// Container Activity must implement this interface
    public interface OnHeadlineSelectedListener {
        public void onButtonClicked(int position);
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnHeadlineSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        
        // onCreateView() is a lifecycle event that is unique to a Fragment. This is called when Android
        // needs the layout for this Fragment. The call to LayoutInflater::inflate() simply takes the layout
        // ID for the layout file, the parent view that will hold the layout, and an option to add the inflated
        // view to the parent. This should always be false or an exception will be thrown. Android will add
        // the view to the parent when necessary.
        View view = inflater.inflate(R.layout.fragment_header_layout, container, false);
        
        // This is how you access your layout views. Notice how we call the findViewById() method
        // on our View directly. There is no method called findViewById() defined on Fragments like
        // there is in an Activity.
        Button button1 = (Button) view.findViewById(R.id.button1);
        Button button2 = (Button) view.findViewById(R.id.button2);
        Button button3 = (Button) view.findViewById(R.id.button3);
        Button button4 = (Button) view.findViewById(R.id.button4);
        
        // A simple OnClickListener for our button. You can see here how a Fragment can encapsulate
        // logic and views to build out re-usable Activity components.
        button1.setOnClickListener(new OnClickListener() {            
            @Override
            public void onClick(View v) {
                Activity activity = getActivity();
                if (activity != null) {
	       			mCallback.onButtonClicked(1);
                }
            }	
        });
        
        button2.setOnClickListener(new OnClickListener() {            
            @Override
            public void onClick(View v) {
                Activity activity = getActivity();
                if (activity != null) {
	       			mCallback.onButtonClicked(2);
                }
            }	
        });
        button3.setOnClickListener(new OnClickListener() {            
            @Override
            public void onClick(View v) {
                Activity activity = getActivity();
                if (activity != null) {
	       			mCallback.onButtonClicked(3);
                }
            }	
        });
        button4.setOnClickListener(new OnClickListener() {            
            @Override
            public void onClick(View v) {
                Activity activity = getActivity();
                if (activity != null) {
	       			mCallback.onButtonClicked(4);
                }
            }	
        });

        
        return view;
    }

	
}
