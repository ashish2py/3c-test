package com.example.c_test;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends ListActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Button moreButton;
		setContentView(R.layout.activity_main);
		
		ListView listView = getListView();
		
		// Add ListView Header
		LayoutInflater inflater = getLayoutInflater();
		final View header = inflater.inflate(R.layout.list_header,null);
		listView.addHeaderView(header);
		
		moreButton = (Button) header.findViewById(R.id.moreAction);
		moreButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Toast.makeText(MainActivity.this, "More action clicked.", Toast.LENGTH_LONG).show();	
				startActivity(new Intent(MainActivity.this, FragmentMainActivity.class));
			}
		});

		String[] value_null =  new String[]{};
		String[] values = new String[] { "Android", "iPhone", "WindowsMobile",
				"Blackberry", "WebOS", "Ubuntu", "Windows7", "Max OS X",
				"Linux", "OS/2" };
		
		SimpleArrayAdapter ardp = null ;
		ardp = new SimpleArrayAdapter(this, R.layout.row_layout,values);
		//listView.setAdapter(ardp);
		setListAdapter(ardp);
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
