package fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.c_test.R;

public class FragList extends Fragment {

  
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        
    	View view = inflater.inflate(R.layout.frag_list_layout, container, false);
        final LinearLayout stringList = (LinearLayout) view.findViewById(R.id.fragmentListLinearLayout);
        
        final TextView initial = (TextView) view.findViewById(R.id.clicked);
        
        // This is how you access your layout views. Notice how we call the findViewById() method
        // on our View directly. There is no method called findViewById() defined on Fragments like
        // there is in an Activity.
        
                String[] values = new String[] { "Android", "iPhone", "WindowsMobile",
                                "Blackberry", "WebOS", "Ubuntu", "Windows7", "Max OS X",
                                "Linux", "OS/2" };
                

        
                int count = values.length;
                final String sCount = String.valueOf(count);
		        for(int i=0;i<count;i++){
		
		                View rowView = inflater.inflate(R.layout.row_layout, container, false);
		                final TextView name = (TextView) rowView.findViewById(R.id.label);
		
		                
		                name.setText(String.valueOf(i)  + " ################################################## ");
		                name.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								name.setText("Huga fire");
			                    Toast.makeText(getActivity(), "chang", Toast.LENGTH_LONG).show();
							}
						});

		                stringList.addView(rowView);    //add items to linear layout
		                
		        }
		        
		        return view;
		    }
}
